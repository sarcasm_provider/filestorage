﻿namespace FileStorage.Exceptions;

public class ExpiredLinkException : Exception
{
    public ExpiredLinkException(string name) : base($"The link to the {name} file expired.")
    {
    }
}