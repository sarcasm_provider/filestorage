﻿namespace FileStorage.Models;

public enum DownloadStatuses
{
    None,
    Uploading,
    Uploaded,
    Error
}