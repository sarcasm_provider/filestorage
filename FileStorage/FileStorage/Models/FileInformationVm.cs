namespace FileStorage.Models;

public class FileInformationVm
{
    public string Filename { get; set; }

    public string FileId { get; set; }
}