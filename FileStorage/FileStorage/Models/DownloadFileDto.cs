﻿namespace FileStorage.Models;

public class DownloadFileDto
{
    public string Filename { get; set; }

    public Stream FileStream { get; set; }
}