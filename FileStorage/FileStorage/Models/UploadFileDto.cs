﻿namespace FileStorage.Models;

public class UploadFileDto
{
    public string Filename { get; set; }

    public string FileId { get; set; }

    public DownloadStatuses Status { get; set; }
}