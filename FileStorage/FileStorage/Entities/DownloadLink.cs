﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FileStorage.Entities;

public class DownloadLink
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }

    public string FileId { get; set; }

    public string Hash { get; set; }

    public DateTime ExpirationTime { get; set; }

    public bool IsFinished { get; set; } = false;
}