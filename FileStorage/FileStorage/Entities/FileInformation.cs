﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FileStorage.Entities;

public class FileInformation
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }

    public string Filename { get; set; }

    public string FileId { get; set; }

    public string ContentType { get; set; }
}