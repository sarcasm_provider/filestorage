﻿using FileStorage.Entities;
using FileStorage.Exceptions;
using FileStorage.Models;
using FileStorage.Persistence;
using FileStorage.Realtime;
using Microsoft.AspNetCore.SignalR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace FileStorage.Services;

public class FileService
{
    private readonly DownloadLinkService _downloadLinkService;
    private readonly IHubContext<DownloadStatusHub> _hubContext;
    private readonly IMongoDbContext _mongoDbContext;

    public FileService(IHubContext<DownloadStatusHub> hubContext, DownloadLinkService downloadLinkService,
        IMongoDbContext mongoDbContext)
    {
        _hubContext = hubContext;
        _downloadLinkService = downloadLinkService;
        _mongoDbContext = mongoDbContext;
    }

    public async Task UploadFilesAsync(IFormFileCollection uploads)
    {
        var filesInfo = new List<FileInformation>();

        var tasks = uploads.Select(upload => Task.Run(() =>
        {
            var fileId = _mongoDbContext.GridFsBucket.UploadFromStream(upload.FileName, upload.OpenReadStream());

            var fileInformation = new FileInformation
                {Filename = upload.FileName, FileId = fileId.ToString(), ContentType = upload.ContentType};

            filesInfo.Add(fileInformation);
        }));

        await Task.WhenAll(tasks);

        var uploadToDbTask =
            Task.Run(() => _mongoDbContext.FilesInfo.InsertManyAsync(filesInfo));
        var sendToHubTask = Task.Run(() => _hubContext.Clients.All.SendAsync("filesInfo", filesInfo));

        await Task.WhenAll(uploadToDbTask, sendToHubTask);
    }

    public async Task<Stream> GetStreamAsync(string fileId)
    {
        return await _mongoDbContext.GridFsBucket.OpenDownloadStreamAsync(new ObjectId(fileId));
    }

    public async Task<IEnumerable<FileInformationVm>> GetAllFilesInfoAsync()
    {
        var filesInfo = await _mongoDbContext.FilesInfo.Find(new FilterDefinitionBuilder<FileInformation>().Empty)
            .ToListAsync();

        return filesInfo.Select(inf => new FileInformationVm {Filename = inf.Filename, FileId = inf.FileId});
    }

    public async Task<FileInformation> GetFilesInfoByFileIdAsync(string fileId)
    {
        return (await _mongoDbContext.FilesInfo.FindAsync(Builders<FileInformation>.Filter.Eq("FileId", fileId)))
            .FirstOrDefault();
    }

    public async Task<DownloadFileDto> GetFileByExpiredLinkAsync(string fileId, string hash)
    {
        var link = await _downloadLinkService.GetLinkAsync(fileId, hash);

        if (link == null) throw new NotFoundException(nameof(DownloadLink), fileId);

        var fileInformation = await GetFilesInfoByFileIdAsync(fileId);

        if (link.IsFinished || link.ExpirationTime < DateTime.UtcNow)
            throw new ExpiredLinkException(fileInformation.Filename);

        return new DownloadFileDto {Filename = fileInformation.Filename, FileStream = await GetStreamAsync(fileId)};
    }
}