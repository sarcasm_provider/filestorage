﻿using System.Security.Cryptography;
using System.Text;
using FileStorage.Entities;
using FileStorage.Persistence;
using MongoDB.Bson;
using MongoDB.Driver;

namespace FileStorage.Services;

public class DownloadLinkService
{
    private readonly IConfiguration _configuration;
    private readonly IMongoDbContext _mongoDbContext;

    public DownloadLinkService(IConfiguration configuration, IMongoDbContext mongoDbContext)
    {
        _configuration = configuration;
        _mongoDbContext = mongoDbContext;
    }

    public async Task<DownloadLink> CreateAsync(string fileId)
    {
        var hash = GetHash(fileId);
        var expirationTime = _configuration.GetSection("LinkExpirationTimeInMinutes").Get<int>();
        var link = new DownloadLink
            {FileId = fileId, ExpirationTime = DateTime.UtcNow.AddMinutes(expirationTime), Hash = hash};

        await _mongoDbContext.DownloadLinks.InsertOneAsync(link);

        return link;
    }

    public async Task<DownloadLink> GetLinkAsync(string fileId, string hash)
    {
        return (await _mongoDbContext.DownloadLinks.FindAsync(new BsonDocument {{"Hash", hash}, {"FileId", fileId}}))
            .FirstOrDefault();
    }

    private string GetHash(string fileId)
    {
        var hash = string.Empty;

        using var md5 = MD5.Create();
        var hashBytes = md5.ComputeHash(Encoding.UTF8.GetBytes(fileId + DateTime.UtcNow));
        hash = BitConverter.ToString(hashBytes).Replace("-", string.Empty);

        return hash;
    }
}