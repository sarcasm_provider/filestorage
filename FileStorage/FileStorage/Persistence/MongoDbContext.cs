﻿using FileStorage.Configs;
using FileStorage.Entities;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace FileStorage.Persistence;

public class MongoDbContext : IMongoDbContext
{
    private readonly IMongoDatabase _database;

    public MongoDbContext(IOptions<MongoDbConfig> setting)
    {
        var client = new MongoClient(setting.Value.ConnectionString);
        _database = client.GetDatabase(setting.Value.DatabaseName);
        GridFsBucket = new GridFSBucket(_database);
    }

    public IMongoCollection<FileInformation> FilesInfo => _database.GetCollection<FileInformation>("file_info");
    public IMongoCollection<DownloadLink> DownloadLinks => _database.GetCollection<DownloadLink>("download_links");
    public IGridFSBucket GridFsBucket { get; }
}