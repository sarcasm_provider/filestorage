﻿using FileStorage.Entities;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace FileStorage.Persistence;

public interface IMongoDbContext
{
    IMongoCollection<FileInformation> FilesInfo { get; }
    IMongoCollection<DownloadLink> DownloadLinks { get; }
    IGridFSBucket GridFsBucket { get; }
}