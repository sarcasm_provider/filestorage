﻿using FileStorage.Models;
using Microsoft.AspNetCore.SignalR;

namespace FileStorage.Realtime;

public class DownloadStatusHub : Hub
{
    public async Task BroadcastStatus(UploadFileDto uploadFile)
    {
        await Clients.All.SendAsync("ReceiveStatus", uploadFile);
    }
}