﻿using FileStorage.Exceptions;
using FileStorage.Services;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver.GridFS;

namespace FileStorage.Controllers;

[ApiController]
[Route("api/[controller]")]
public class FilesController : ControllerBase
{
    private readonly DownloadLinkService _downloadLinkService;
    private readonly FileService _fileService;

    public FilesController(FileService fileService, DownloadLinkService downloadLinkService)
    {
        _fileService = fileService;
        _downloadLinkService = downloadLinkService;
    }

    [HttpPost("upload")]
    [DisableRequestSizeLimit]
    [RequestFormLimits(MultipartBodyLengthLimit = int.MaxValue,
        ValueLengthLimit = int.MaxValue)]
    public async Task<ActionResult> Upload(IFormFileCollection uploads)
    {
        await _fileService.UploadFilesAsync(uploads);

        return Ok();
    }

    [HttpGet("download")]
    public async Task<IActionResult> Download(string fileName, string fileId)
    {
        try
        {
            return File(await _fileService.GetStreamAsync(fileId), "application/octet-stream", fileName);
        }
        catch (GridFSFileNotFoundException e)
        {
            return NotFound($"File {fileName} not found");
        }
    }

    [HttpGet("external-download", Name = "DownloadByExternalLink")]
    public async Task<IActionResult> DownloadByExternalLink(string fileId, string hash)
    {
        try
        {
            var downloadFileDto = await _fileService.GetFileByExpiredLinkAsync(fileId, hash);

            return File(downloadFileDto.FileStream, "application/octet-stream", downloadFileDto.Filename);
        }
        catch (ExpiredLinkException e)
        {
            return Ok(e.Message);
        }
        catch (NotFoundException e)
        {
            return NotFound(e.Message);
        }
    }

    [HttpGet]
    public async Task<ActionResult> GetAll()
    {
        var filesInfo = await _fileService.GetAllFilesInfoAsync();

        return Ok(filesInfo);
    }

    [HttpGet("link")]
    public async Task<ActionResult> GetLink(string fileId)
    {
        var linkData = await _downloadLinkService.CreateAsync(fileId);

        return Ok(Url.Link("DownloadByExternalLink", new {fileId, hash = linkData.Hash}));
    }
}