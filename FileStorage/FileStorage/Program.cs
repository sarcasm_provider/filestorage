using FileStorage.Configs;
using FileStorage.Persistence;
using FileStorage.Realtime;
using FileStorage.Services;

var builder = WebApplication.CreateBuilder(args);

IConfiguration configuration = builder.Configuration;

// Add services to the container.
builder.Services.AddSignalR();
builder.Services.AddScoped<FileService>();
builder.Services.AddScoped<DownloadLinkService>();
builder.Services.AddSingleton<IMongoDbContext, MongoDbContext>();

builder.Services.Configure<MongoDbConfig>(options =>
{
    options.ConnectionString = configuration.GetSection("MongoDbSettings:ConnectionString").Value;
    options.DatabaseName = configuration.GetSection("MongoDbSettings:DatabaseName").Value;
});

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();

app.MapHub<DownloadStatusHub>("/statuses");

app.Run();