﻿namespace FileStorage.Configs;

public class MongoDbConfig
{
    public string DatabaseName { get; set; }
    public string ConnectionString { get; set; }
}